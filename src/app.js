const { fork } = require('child_process');
const readline = require('readline');
const wav = require('node-wav');
const path = require('path');
const fs = require('fs');

let workers = [];

let frame = 0;
let frames = 6572;
let step = Math.ceil(frames/12);
for (let i = 0; i < 12; i++) {
  workers.push({
    process: fork(path.join(__dirname, 'worker.js')),
    status: "Waiting",
    started: Date.now()
  });
}

console.clear();
setInterval(() => {
  readline.cursorTo(process.stdout, 0, 0);
  for (let i = 0; i < 12; i++) {
    let id = i.toString().padStart(2, '0');
    process.stdout.write(`[Worker ${id}] ${workers[i].status} (${Date.now() - workers[i].started}ms)`);
    readline.clearLine(process.stdout, 1);
    process.stdout.write('\n');
  }
  process.stdout.write(`Frame ${frame} / ${frames}`);
  readline.clearLine(process.stdout, 1);
  process.stdout.write('\n');
}, 250);

const finishedFrames = new Array(frames);

Promise.all(workers.map((worker, i) => new Promise((res, rej) => {
  if (frame > frames) return res();
  worker.started = Date.now();
  worker.process.send(JSON.stringify({ frame: frame++ }));
  worker.process.on('message', json => {
    let msg = JSON.parse(json);

    if (msg.type == 'frame') {
      finishedFrames[msg.frame] = [msg.l, msg.r];

      frame++;
      if (frame > frames) {
        res();
        worker.process.send(JSON.stringify({ type: 'finish' }));
        return;
      }
      worker.started = Date.now();
      worker.process.send(JSON.stringify({ type: 'frame', frame: frame-1 }));
    }

    if (msg.type == 'status') {
      worker.status = msg.status;
      statusesDirty = true;
    }
  });
  worker.process.on('error', ex => rej(ex));
}))).then(results => {
  console.log("Performing final encoding...");
  let l = [];
  let r = [];
  for (let frame of finishedFrames) {
    l.push(...frame[0]);
    r.push(...frame[1]);
  }
  let wave = wav.encode([l, r], {
    sampleRate: 44100,
    float: true,
    bitDepth: 32
  });
  let file = `out.wav`;
  fs.writeFileSync(file, wave);
  process.exit(0);
}).catch(console.error);
