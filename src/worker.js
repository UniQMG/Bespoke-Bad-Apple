const { Canvas, loadImage } = require('canvas');

const fps = 30;
const overdraw = 2;
const sampleRate = 44100;

process.on('message', async json => {
  let { type, frame } = JSON.parse(json);
  if (type == 'finish') process.exit();
  let l = [];
  let r = [];

  function status(msg) {
    process.send(JSON.stringify({
      type: 'status',
      status: `Frame #${frame.toString().padStart(4, '_')}: ${msg}`
    }));
  }

  status("Loading");
  let img = await loadImage(`frames/image-${(frame+1).toString().padStart(7, '0')}.png`);
  let canvas = new Canvas(256, 192);
  let ctx = canvas.getContext('2d');
  ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

  status("Converting to 1 bit");
  require('./fn/one-bit')(canvas, ctx);
  status("Edge detect");
  let edges = require('./fn/trace-outline')(canvas, ctx);
  status("Ensuring followable paths");
  require('./fn/ensure-followable-paths')(canvas, ctx, edges, msg => status(`Ensuring followable paths [${msg}]`));
  status("Generating paths");
  let paths = require('./fn/follow-outline')(canvas, ctx, edges);

  status("Sampling")
  let path_x = [];
  let path_y = []

  for (let path of paths) {
    for (let [x,y] of path) {
      path_x.push(x);
      path_y.push(y);
    }
  }

  function lerp(x1, x2, i) {
    return x1 + (x2 - x1) * i;
  }
  let step = sampleRate / (fps * overdraw);
  for (let i = 0; i < path_x.length; i += path_x.length / step) {
    let imod = i % path_x.length;
    let x = lerp(path_x[Math.floor(imod)], path_x[Math.ceil(imod)], imod%1);
    let y = lerp(path_y[Math.floor(imod)], path_y[Math.ceil(imod)], imod%1);
    l.push((x / canvas.width  - 0.5) * 2);
    r.push((y / canvas.height - 0.5) * 2);
  }

  status("Done");
  process.send(JSON.stringify({ type: 'frame', frame, l, r }));
});
