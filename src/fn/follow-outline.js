module.exports = function followOutline(canvas, ctx, edges) {
  function findStart() {
    while (true) {
      if (!edges.length) return null;
      let [x, y] = edges.pop();
      let [r,g,b,a] = ctx.getImageData(x, y, 1, 1).data;
      if (r == 255 && g == 0 && b == 0) return [x, y];
    }
  }

  let dirs = [[0, 1], [-1, 0], [0, -1], [1, 0]];
  let colors = ['#0000FF', '#007700', '#00FFFF', '#00FF77', '#0077FF'];
  let paths = [];

  while (true) {
    let next = findStart();
    if (!next) break;
    let [x, y] = next;
    ctx.fillStyle = colors.pop() || '#007777';
    let path = [];

    walk: while (true) {
      path.push([x, y]);
      ctx.fillRect(x, y, 1, 1);
      for (let [dx, dy] of dirs) {
        if (x+dx < 0 || x+dx >= canvas.width || y+dy < 0 || y+dy >= canvas.height)
          continue;
        let [r,g,b,a] = ctx.getImageData(x+dx, y+dy, 1, 1).data;
        if (r == 255 && g == 0 && b == 0) {
          x += dx;
          y += dy;
          continue walk;
        }
      }
      break;
    }

    paths.push(path);
  }
  return paths;
}
