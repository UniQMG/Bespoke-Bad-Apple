/**
 * Resolves pixels with more than two neighbors
 */
module.exports = function ensureFollowablePaths(canvas, ctx, edges, status) {
  const px = (x, y) => {
    if (x < 0 || y < 0 || x >= canvas.width || y >= canvas.height)
      return [0, 0, 0, 0];
    return ctx.getImageData(x, y, 1, 1).data;
  };
  const is = (x,y,r,g,b) => {
    let [ir, ig, ib, ia] = px(x, y);
    return (r == ir) && (g == ig) && (b == ib);
  }
  const dirs = [[0, 1], [-1, 0], [0, -1], [1, 0]];

  function neighbors(x, y) {
    let neighbors = 0;
    for (let [dx,dy] of dirs) {
      if (is(x+dx, y+dy, 255, 0, 0))
        neighbors++;
    }
    return neighbors;
  }

  for (let i = 0; i < edges.length; i++) {
    if (i % 100 == 0)
      status(`${i}/${edges.length}`);
    let [x,y] = edges[i];

    if (!is(x, y, 255, 0, 0)) continue;
    if (neighbors(x, y) <= 2) continue;

    let start = null;
    for (let dx = -1; dx <= 1; dx++) {
      for (let dy = -1; dy <= 1; dy++) {
        if (!is(x+dx, y+dy, 255, 0, 0)) continue;
        ctx.fillStyle = '#00FF00';
        ctx.fillRect(x+dx, y+dy, 1, 1);
      }
    }
    findStart: for (let dx = -1; dx <= 1; dx++) {
      for (let dy = -1; dy <= 1; dy++) {
        if (!is(x+dx, y+dy, 0, 255, 0)) continue;
        if (neighbors(x+dx, y+dy) == 1) {
          function pathfind(path) {
            // BUG: sometimes takes an extraordinarily long amount of time
            // (e.g. frame 523), possibly even an infinite loop?
            // Just bail on these, the path will never be longer than 9px ever
            // (possibly an issue with muliple findStarts drawing on to the canvas and overlapping?)
            if (path.length > 10)
              return { length: Infinity };

            if (path.length > 1) {
              let [x, y] = path.slice(-1)[0];
              for (let [dx,dy] of dirs)
                if (is(x+dx, y+dy, 255, 0, 0))
                  return [...path, [x+dx, y+dy]]; // finished!
            }
            const [x, y] = path.slice(-1)[0];
            let final = dirs
              .filter(([dx, dy]) => {
                if (!is(x+dx, y+dy, 0, 255, 0))
                  return false; // Not visitable
                for (let [px, py] of path)
                  if (px == x+dx && py == y+dy)
                    return false; // Already visited
                return true;
              })
              .map(([dx, dy]) => pathfind([...path, [x+dx, y+dy]]))
              .reduce((pA, pB) => pA.length <= pB.length ? pA : pB, { length: Infinity });
            return final;
          }
          let path = pathfind([[x+dx, y+dy]]);
          if (path.length == Infinity) {
            ctx.fillStyle = 'red';
            for (let dx = -1; dx <= 1; dx++)
              for (let dy = -1; dy <= 1; dy++)
                if (is(x+dx, y+dy, 0, 255, 0))
                  ctx.fillRect(x+dx, y+dy);
          } else {
            for (let el of path) {
              ctx.fillStyle = 'red';
              ctx.fillRect(...el, 1, 1);
            }
          }
          break findStart;
        }
      }
    }
  }
}
