module.exports = function convertToOneBit(canvas, ctx) {
  let imgdata = ctx.getImageData(0, 0, canvas.width, canvas.height);
  for (let i = 0; i < imgdata.data.length; i += 4) {
    let [r, g, b, a] = imgdata.data.slice(i, i+4);
    let gr = (0.2126*r/255 + 0.7152*g/255 + 0.0722*b/255) * a/255;
    [r, g, b, a] = gr > 0.95 ? [255, 255, 255, 255] : [0, 0, 0, 255];
    imgdata.data.set([r, g, b, a], i);
  }
  ctx.putImageData(imgdata, 0, 0);
}
