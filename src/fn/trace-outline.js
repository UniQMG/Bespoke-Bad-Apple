module.exports = function traceOutline(canvas, ctx) {
  let imgdata = ctx.getImageData(0, 0, canvas.width, canvas.height);
  
  function trace(x, y) {
    let data = new Array(9);
    for (let dx = 0; dx < 3; dx++) {
      for (let dy = 0; dy < 3; dy++) {
        let ax = x + dx - 1;
        let ay = y + dy - 1;
        if (ax < 0 || ax >= canvas.width || ay < 0 || ay >= canvas.height) {
          data[dy*3+dx] = true;
          continue;
        }
        let i = (ay * canvas.width + ax) * 4;
        let [r,g,b,a] = imgdata.data.slice(i, i+4);
        data[dy*3+dx] = r == 255 && g == 255 && b == 255;
      }
    }
    return data;
  }

  let edges = [];
  for (let x = 0; x < canvas.width; x++) {
    for (let y = 0; y < canvas.height; y++) {
      let t = 0, f = 0;
      for (let px of trace(x, y))
        if (px) t++; else f++;
      if (!(t && f)) continue;
      let [r,g,b,a] = ctx.getImageData(x, y, 1, 1).data;
      if (r == 255) continue; // only trace black borders

      ctx.fillStyle = 'red';
      ctx.fillRect(x, y, 1, 1);
      edges.push([x, y]);
    }
  }

  return edges;
}
