Vectorizes Bad Apple!! into a format suitable for playing in Bespoke

[![Alt text](https://img.youtube.com/vi/D5Qo7gL0Iwg/0.jpg)](https://www.youtube.com/watch?v=D5Qo7gL0Iwg)

## Setup
```bash
youtube-dl https://www.youtube.com/watch?v=FtutLA63Cp8
mkdir frames
ffmpeg -i badapple.mkv -f image2 frames/image-%07d.png
yarn
node index.js
```

Drag `out.wav` to a Bespoke `sampleplayer` connected to a `lissajous`.
Make sure to disable 'autocorrelation' in the `lissajous` arrow menu.
